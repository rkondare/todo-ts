
import React, { SetStateAction } from 'react';
import { useState } from 'react';
import { Button, FormControl, InputGroup } from 'react-bootstrap';
import Form from 'react-bootstrap/esm/Form';
import './page.css'

function Home() {

  const todoData = [
    {
      name: "Rohit ",
      lastName: "Kandare"
    }
  ]

  const todoArr: SetStateAction<{ name: string; lastName: string; }[]> = []
  const [todo, setTodo] = useState(todoData)
  const [data, setData] = useState({ name: '', lastName: '' })

  const handleChange = (event: { target: { name: any; value: any; }; }) => {
    const { name, value } = event.target
    setData({
      ...data,
      [name]: value
    })
  }

  const addTodo = (event: { preventDefault: () => void; }) => {
    event.preventDefault()
    const values: any = [...todo, data]
    setTodo(
      values
    )
  }

  const deleteTodo = (id: number) => {
    todo.filter((value, index) => {
      if (index !== id) {
        todoArr.push(value)
      }
    })
    setTodo(todoArr)
  };

  const editeTodo = (id: number) => {
    todo.filter((value, index) => {
      if (index === id) {
        setData(value)
      }
      if (index !== id) {
        todoArr.push(value)
      }
    })
    setTodo(todoArr)
  };
  return (
    <>
      <div className="tableSetting">
        <Form inline onSubmit={addTodo}>
          <Form.Label htmlFor="inlineFormInputName2" srOnly>
            Name
       </Form.Label>
          <Form.Control
            className="mb-2 mr-sm-2"
            id="inlineFormInputName2"
            name='name'
            value={data.name}
            onChange={handleChange}

          />
          <Form.Label htmlFor="inlineFormInputGroupUsername2" srOnly>
            LastName
        </Form.Label>
          <InputGroup className="mb-2 mr-sm-2">
            <FormControl id="inlineFormInputGroupUsername2" name='lastName' value={data.lastName} onChange={handleChange} />
          </InputGroup>
          <Button type="submit" className="mb-2">
            Submit
         </Button>
        </Form>
        <table className="table">
          <thead className="thead-light">
            <tr>
              <th scope="col">SNO.</th>
              <th scope="col">First</th>
              <th scope="col">Last</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {todo.map((data, index) => {
              return (<tr key={index}>
                <th scope="row" key={index}>{index + 1}</th>
                <td>{data.name}</td>
                <td>{data.lastName}</td>
                <td>
                  <div className="btn-group">
                    <button className="btn btn-warning mr-2" onClick={() => { editeTodo(index) }}>Edit</button>
                    <button className="btn btn-danger mr-2" onClick={() => { deleteTodo(index) }}>Delete</button>
                  </div>
                </td>
              </tr>)
            })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default Home;
